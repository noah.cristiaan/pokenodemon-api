const chai = require('chai');

const expect = chai.expect;

var chaiAsPromised = require('chai-as-promised');
const { DateTime } = require('neo4j-driver');

chai.use(chaiAsPromised);

const User = require('../../src/models/user.schema')();

describe('user model',function (){
    it('should reject missing user name', async function (){
        const user = new User({
            birthdate: Date.now(),
            password: 'validpassword123',
           // firstname:'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.cristiaan@gmail.com',
            userGender: 'Male',
            role: 'Admin',
            pokemonTeam:[]
              
        })
        await expect(user.save()).to.be.rejectedWith(Error)
    })
    it('should reject missing email', async function (){
        const user = new User({
            birthdate: Date.now(),
            password: 'validpassword123',
            firstname:'Noah',
            lastname: 'de Keijzer',
            // emailaddress: 'noah.cristiaan@gmail.com',
            userGender: 'Male',
            role: 'Admin',
            pokemonTeam:[]
              
        })
        await expect(user.save()).to.be.rejectedWith(Error)
    })
    it('should reject missing password', async function (){
        const user = new User({
            birthdate: Date.now(),
            // password: 'validpassword123',
            firstname:'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.cristiaan@gmail.com',
            userGender: 'Male',
            role: 'Admin',
            pokemonTeam:[]
              
        })
        await expect(user.save()).to.be.rejectedWith(Error)
    })
    it('should reject missing birthdate', async function (){
        const user = new User({
            birthdate: Date.now(),
            password: 'validpassword123',
            firstname:'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.cristiaan@gmail.com',
            userGender: 'Male',
            role: 'Admin',
            pokemonTeam:[]
              
        })
        await expect(user.save()).to.be.rejectedWith(Error)
    })
    it('should reject missing pokemonteam', async function (){
        const user = new User({
            birthdate: Date.now(),
            password: 'validpassword123',
            firstname:'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.cristiaan@gmail.com',
            userGender: 'Male',
            role: 'Admin',
            // pokemonTeam:[]
              
        })
        await expect(user.save()).to.be.rejectedWith(Error)
    })
    it('should reject missing role', async function (){
        const user = new User({
            birthdate: Date.now(),
            password: 'validpassword123',
            firstname:'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.cristiaan@gmail.com',
            userGender: 'Male',
            // role: 'Admin',
            pokemonTeam:[]
              
        })
        await expect(user.save()).to.be.rejectedWith(Error)
    })
    it('should reject missing gender', async function (){
        const user = new User({
            birthdate: Date.now(),
            password: 'validpassword123',
            firstname:'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.cristiaan@gmail.com',
            // userGender: 'Male',
            role: 'Admin',
            pokemonTeam:[]
              
        })
        await expect(user.save()).to.be.rejectedWith(Error)
    })
    it('should save', async function (){
        const user = new User({
            birthdate: Date.now(),
            password: 'validpassword123',
            firstname:'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.cristiaan@gmail.com',
            userGender: 'Male',
            role: 'Admin',
            pokemonTeam:[]
              
        })
        await expect(user.save())
    })
})
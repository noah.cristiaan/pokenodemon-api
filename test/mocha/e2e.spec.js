const chai = require('chai');
const expect = chai.expect;
const app = require('../../src/app');

const User = require('../../src/models/user.schema')();

var requester = require('../requester')

const connect = require("../../connect");

before(async function () {
    await connect.mongo()
})

describe('user endpoint', function () {
    let restoken;
    beforeEach(async () => {
        await Promise.all([User.deleteMany()])
        restoken = await requester.post('/users').send({
            birthdate: Date.now,
            password: 'validpassword123',
            firstname: 'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.cristiaan@gmail.com',
            userGender: 'Male',
            role: 'Admin',
            pokemonTeam: []
        })
    })
    it('e2e test user should be created, result should be 201', async () =>{
        const result = await requester.post('/users').send({
            birthdate: Date.now(),
            password: 'validpassword123',
            firstname: 'Noah',
            lastname: 'de Keijzer',
            emailaddress: 'noah.crisjtiaan@gmail.com',
            userGender: 'Male',
            role: 'Admin',
            pokemonTeam: []
        })
        expect(result).to.have.status(201);
    })
})
after(()=> {
    requester.close()
})
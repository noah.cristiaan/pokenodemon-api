const User = require('../../src/models/user.schema')(); // note we need to call the model caching function

const AuthenticationController = require('../../src/controllers/AUTHcontroller');

const AuthController = new AuthenticationController(User);

const connect = require("../../connect");
const jest = require("jest-mock");
const chai = require('chai');
const expect = chai.expect;

beforeAll(async () => {
  await connect.mongo();
  await Promise.all([User.deleteMany()]);
});

before(async () => {
  await connect.mongo();
  await Promise.all([User.deleteMany()]);
});

describe('integration auth tests', () => {
  it('should return 200 status code along with token and user details when the user registers successfully', async () => {
    const req = {
      body: {
        emailaddress: 'jane.doe@example.com',
        password: 'password123',
        firstname: 'Jane',
        lastname: 'Doe',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const next = jest.fn();

    await AuthController.register(req, res, next);
  
    var json = res.json.mock.lastCall[0]
    expect(res.status.mock.lastCall[0]).equal(200);
    expect(json.token).to.be.an('string')
    expect(json.user).to.be.an('object')
    expect(json.expires).to.be.an('date')
  });

  it('should return an error when there is a duplicate email address', async () => {
    const req = {
      body: {
        emailaddress: 'jane.doe@example.com',
        password: 'password123',
        firstname: 'Johny',
        lastname: 'Doe',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const next = jest.fn();

    await AuthController.register(req, res, next);

    expect(res.status.mock.lastCall[0]).equal(400);
  });

  it('should return 401 status code when user with given email address is not found', async () => {
    const req = { body: { email: 'someone@example.com', password: 'password123' } };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const next = jest.fn();

    await AuthController.login(req, res, next);

    expect(res.status.mock.lastCall[0]).equal(401);
      
    var json = res.json.mock.lastCall[0]
    expect(json.message).to.be.an('string',  'User with this Email does not exist')
  });


  it('should return 401 status code when the user provides invalid password', async () => {
    const req = { body: { email: 'jane.doe@example.com', password: 'invalidpass' } };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),

    }
    const next = jest.fn();

    await AuthController.login(req, res, next);

    expect(res.status.mock.lastCall[0]).equal(401);
  })
})

afterAll(() => {;
  Promise.all([User.deleteMany()]);
});

after(() => {
  Promise.all([User.deleteMany()]);
});
const express = require('express')
const router = express.Router()

const Item = require('../models/item.schema')() // note we need to call the model caching function
const User = require('../models/user.schema')()

const CrudController = require('../controllers/CRUDcontroller')
const AuthenticationController = require('../controllers/AUTHcontroller')

const ItemCrudController = new CrudController(Item)
const AuthController = new AuthenticationController(User)
// create a Move
router.post('/',  AuthController.validate, ItemCrudController.create)
// get all Move
router.get('/',  AuthController.validate, ItemCrudController.getAll)
// get a Move
router.get('/:id', AuthController.validate, ItemCrudController.getOne)
// update a Move
router.put('/:id',  AuthController.validate, ItemCrudController.update)
// remove a Move
router.delete('/:id',  AuthController.validate, ItemCrudController.delete)

module.exports = router;

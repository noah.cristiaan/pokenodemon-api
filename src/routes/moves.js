const express = require('express')
const router = express.Router()

const Move = require('../models/move.schema')() // note we need to call the model caching function
const User = require('../models/user.schema')()

const CrudController = require('../controllers/CRUDcontroller')
const AuthenticationController = require('../controllers/AUTHcontroller')

const MoveCrudController = new CrudController(Move)
const AuthController = new AuthenticationController(User)

// create a Move
router.post('/',  AuthController.validate, MoveCrudController.create)
// get all Move
router.get('/',  AuthController.validate, MoveCrudController.getAll)
// get a Move
router.get('/:id', AuthController.validate, MoveCrudController.getOne)
// update a Move
router.put('/:id', AuthController.validate, MoveCrudController.update)
// remove a Move
router.delete('/:id', AuthController.validate, MoveCrudController.delete)

module.exports = router;

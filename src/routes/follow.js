const express = require('express')
const router = express.Router()

const User = require('../models/user.schema')() // note we need to call the model caching function

const FollowController = require('../controllers/followController')

const UserFollowController = new FollowController(User)
// Add a following
router.post('', function (req,res,next) {
    UserFollowController.AddFollowing(req,res,next)
})
// Get all following
// router.get('/:id', UserFollowController.Ge)

module.exports = router;

const express = require('express')
const router = express.Router()

const User = require('../models/user.schema')() // note we need to call the model caching function

const AuthenticationController = require('../controllers/AUTHcontroller');

const AuthController = new AuthenticationController(User)

// login a user
router.post('/login', AuthController.login)
// register all users
router.post('/register',AuthController.register)


module.exports = router;

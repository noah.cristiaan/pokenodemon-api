const express = require('express')
const router = express.Router()

const Pokemon = require('../models/pokemon.schema')() // note we need to call the model caching function
const User = require('../models/user.schema')()

const CrudController = require('../controllers/CRUDcontroller')
const AuthenticationController = require('../controllers/AUTHcontroller')

const AuthController = new AuthenticationController(User)
const PokemonCrudController = new CrudController(Pokemon)

// create a Pokemon
router.post('/', AuthController.validate, PokemonCrudController.create)
// get all Pokemons
router.get('/', AuthController.validate, PokemonCrudController.getAll)
// get a Pokemon
router.get('/:id', AuthController.validate, PokemonCrudController.getOne)
// update a Pokemon
router.put('/:id',  AuthController.validate, PokemonCrudController.update)
// remove a Pokemon
router.delete('/:id', AuthController.validate, PokemonCrudController.delete)

module.exports = router;

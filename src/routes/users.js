const express = require('express')
const router = express.Router()

const User = require('../models/user.schema')() // note we need to call the model caching function

const CrudController = require('../controllers/CRUDcontroller')
const FollowController = require('../controllers/followController')
const AuthenticationController = require('../controllers/AUTHcontroller')

const UserFollowController = new FollowController(User)
const UserCrudController = new CrudController(User)
const AuthController = new AuthenticationController(User)

// create a user
router.post('/',  AuthController.validate, UserCrudController.create)
// get all users
router.get('/',  AuthController.validate, UserCrudController.getAll)
// get a user
router.get('/:id', AuthController.validate, UserCrudController.getOne)
// update a user 
router.put('/:id', AuthController.validate, UserCrudController.update)
// remove a user
router.delete('/:id', AuthController.validate, UserCrudController.delete)
// add friend
router.post('/follow', function (req,res,next){ 
    AuthController.validate,
    UserFollowController.AddFollowing(req,res,next)
})
// get following
router.get('/follow/:currentUserId', function(req,res,next){ 
    UserFollowController.GetFollowing(req,res,next)
})
// router.post()
module.exports = router;

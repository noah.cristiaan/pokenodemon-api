const neo4jDriver = require('../neo')

class FollowController {
  constructor(model) {
    this.model = model
  }

  AddFollowing = async (req, res, next) => { 
    neo4jDriver.connect()
    const session = neo4jDriver.session();
    let userToFollow = this.model.findById(req.body.userToFollowId)
    await session.run(neo4jDriver.addFollowing, {
      followUser: userToFollow.firstname + ' ' + userToFollow.lastname,
			followUserId: req.body.userToFollowId,
			currentUserId: req.body.currentUserId,
		});

    res.status(201).send("Succes");
  }

  GetFollowing = async (req, res, next) => { 
    neo4jDriver.connect()
    const session = neo4jDriver.session();
    let result = '';
		let users = [];
    try {
			result = await session.run(neo4jDriver.getFollowing, {
				currentUserId: req.params.currentUserId,
			});
		} catch (error) {
			res.status(404).send();
		}
		for (let i = 0; i < result.records.length; i++) {
			users.push(await this.model.findById(result.records[i]._fields[0].properties.userId));
		}
		res.status(200).json(users);
  }
}
module.exports = FollowController;
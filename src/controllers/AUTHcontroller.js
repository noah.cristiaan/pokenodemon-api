// this contains all basic CRUD endpoints on a schema

const errors = require("../errors");
const ErrorController = require('../controllers/errorController')
const jwt = require("node-jsonwebtoken");
const fs = require("fs");
const bcrypt = require("bcrypt");
const neo = require("../neo");
const { ObjectId } = require("mongodb");
const RSA_PRIVATE_KEY = fs.readFileSync("jwtRS256.key");
const model = require("../models/user.schema")();
// the schema is supplied by injection
class controllerAuthentication {
  constructor(model) {
    this.model = model;
  }

  // dit is Noah's code
  async login(req, res, next) {
    const user = await model.findOne({ emailaddress: req.body.email });

    if (user == null) {
      return res.status(401).json({
        message: "User with this Email does not exist",
      });
    }
    if (req.body.password == user.password) {
      const token = jwt.sign(await user.toJSON(), RSA_PRIVATE_KEY);
      return res.status(200).json({
        message: "Login succesful",
        user,
        token,
      });
    } else {
      return res.status(401).json({
        message: "invalid password",
      });
    }
  }
  async register(req, res, next) {
    delete req.body._id;
    let user = new model(req.body);

    user.password = await bcrypt.hash(req.body.password, 10);
    const userFound = await model.findOne({ emailaddress: req.body.email });

    if (!userFound) {
      try {
        await user.save();
      } catch (error) {
        console.log(error);
        ErrorController.handleError(res, error);
        return;
      }
    }else{
      return res.status(401).json({
        message: "User with this Email already exists",
      });
    }
    console.log("saving user", user);
    if (user) {
      console.log("if user", user);
      const jwtBearerToken = jwt.sign({}, RSA_PRIVATE_KEY, {
        expiresIn: "1d",
        subject: user._id.toString(),
      });

      const today = new Date();
      const date = new Date();
      date.setDate(today.getDate() + 1);

      // Add the user in the neo db
      neo.connect();
      const session = neo.session();

      // console.log(user._id.toString());
      await session.run(neo.addUser, {
        userName: user.firstname + " " + user.lastname,
        userId: user._id.toString(),
      });

      res.status(200).json({
        token: jwtBearerToken,
        expires: date,
        user,
      });
    }
  }
  validate = (req, res, next) => {
    try {
      const token = req.headers.authorization.split(" ")[1];
      const secret = RSA_PRIVATE_KEY;
      console.log(secret);
      console.log(token);
      jwt.verify(token, secret);
      req.user = jwt.decode(token);
      next();
    } catch (e) {
      return res.status(401).send({
        code: 401,
        error: "Unauthorized ",
        message: "You are not signed in",
      });
    }
  };
}

module.exports = controllerAuthentication;

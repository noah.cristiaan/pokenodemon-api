const neo4j = require("neo4j-driver");
const env = require('dotenv').config().parsed;

function connect(dbName) {
  this.dbName = dbName;
  if ((dbName = "neo4j")) {
    let url = env.NEO4J_URL;
    let user = env.NEO4J_USER;
    let password = env.NEO4J_PASSWORD;

    this.driver = neo4j.driver(
      url,
      neo4j.auth.basic(user, password)
    );
  } else {
    this.driver = neo4j.driver(
      url,

      neo4j.auth.basic(
        user,
        password
      )
    );
  }
}

function session() {
  return this.driver.session({
    database: this.dbName,
    defaultAccessMode: neo4j.session.WRITE,
  });
}

module.exports = {
  connect,
  session,
  addUser: "CREATE (you:Person {name:$userName, userId:$userId})",
  addFollowing:
    "MATCH (a:Person) where a.userId = $currentUserId MATCH (b:Person) where b.userId = $followUserId WITH a, b MERGE  (a) - [:Follows] - > (b)",
  getFollowing: "MATCH (:Person {userId: $currentUserId})-[r]-(d) RETURN d",
  dropAll: "MATCH (n) DETACH DELETE n",
};

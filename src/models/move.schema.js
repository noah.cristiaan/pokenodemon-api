const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const getModel = require("./model_cache.js")

const MoveSchema = new Schema({
  id: Number,
  name: String,
  statusEffect: String,
  type: String,
  accuracy: Number,
  damage: Number,
});

module.exports = getModel("Move", MoveSchema);


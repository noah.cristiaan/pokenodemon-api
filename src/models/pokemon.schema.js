const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getModel = require("./model_cache.js");

const PokemonType = {
  NORMAL: String,
  FIRE: String,
  WATER: String,
  ELETRIC: String,
  GRASS: String,
  ICE: String,
  FIGHTING: String,
  POISON: String,
  GROUND: String,
  FLYING: String,
  PSYCHIC: String,
  BUG: String,
  ROCK: String,
  GHOST: String,
  DRAGON: String,
  DARK: String,
  STEEL: String,
  FAIRY: String,
  NONE: String
};

const PokemonSchema = new Schema({
  pokemonImageURL: {
    type: String,
    required: [true, 'A pokemon needs to have a picture.'],
  },
  name: {
    type: String,
    required: [true, 'A pokemon needs to have a name.'],
  },
  types: [
    {
      type: String,
      required: [true, 'A pokemon needs to have a types.'],
    },
  ],
  moves: [
    {
      move: { type: mongoose.Schema.Types.ObjectId, ref: 'Moves' },
    }
  ],
  nature: {
    type: String,
    required: [true, 'A pokemon needs to have a nature.'],
  },
  ability: {
    type: String,
    required: [true, 'A pokemon needs to have a ability.'],
  },
  item: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Items'
  }
});

module.exports = getModel("Pokemon", PokemonSchema);


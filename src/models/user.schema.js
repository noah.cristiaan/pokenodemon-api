const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getModel = require("./model_cache.js");

const UserSchema = new Schema({
  birthdate: Date,
  password: String,
  firstname: {
    type: String,
    required: [true, 'A user needs to have a first name.'],
  },
  lastname: {
    type: String,
    required: [true, 'A user needs to have a last name.'],
  },
  
  emailaddress: {
    type: String,
    unique: [true, 'A user needs to have a unique email'],
    required: [true, 'A user needs to have a email.'],
  },
  userGender: String,
  role: String,
  pokemonTeam: [{
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pokemon'
    },
    pokemonImageURL: String,
    name: String,
    types: [
        {
            type: String
        },
    ],
    moves: [
        {   
            type: mongoose.Schema.Types.ObjectId, ref: 'Moves' ,
        }
    ],
    nature: String,
    ability: String,
    item: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Items'
    }
}]
});

module.exports = getModel("User", UserSchema);

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const getModel = require("./model_cache.js")

const ItemSchema = new Schema({
  id: String,
  name: String,
  description: String,
  imageItem: String
});

module.exports = getModel("Item", ItemSchema);

